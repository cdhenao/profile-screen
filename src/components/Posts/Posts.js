import React, { Component } from 'react';
import Post from '../Post/Post'
import './Posts.sass'

/**
 * @author Cristhiam Henao
 * @description This class handles the posts to be showed
 */
class Posts extends Component {
  /**
  * @description It renders a post from user
  * @param {object} user object that contains information about user (name, description, favorite avenger, phone, profile picture) 
  * @param {array} posts list of posts from user
  */
  render(){
    const {posts, user} = this.props

    return (
      <div className="posts">
        {
          posts.map((post,key)=>{
            return <Post user={user} post={post} key={key}/>
          })
        }
      </div>
    )
  }
}

export default Posts