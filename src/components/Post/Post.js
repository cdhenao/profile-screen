import React, { Component } from 'react';
import './Post.sass'

/**
 * @author Cristhiam Henao
 * @description This class shows a single post content and user name and profile
 */
class Post extends Component{
  /**
  * @description It renders a post from user
  * @param {object} user object that contains information about user (name, description, favorite avenger, phone, profile picture) 
  * @param {object} post post text to be rendered
  */
  render(){
    const {post, user} = this.props

    return(
      <div className="post">
        <div className="profileDiv">
          <img alt="profile" src={user.profile}/>
        </div>
        <div className="info">
          <p className="name">{user.name}</p>
          <p className="text">{post.text}</p>
        </div>
      </div>
    )
  }
}

export default Post