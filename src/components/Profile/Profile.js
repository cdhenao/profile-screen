import React, { Component } from 'react';
import UserScreen from '../UserScreen/UserScreen'
import UserEdit from '../UserEdit/UserEdit'
import Posts from '../Posts/Posts'
import iron from '../../assets/iron.png'
import america from '../../assets/america.png'
import hawkeye from '../../assets/hawkeye.png'
import hulk from '../../assets/hulk.png'
import thor from '../../assets/thor.png'
import widow from '../../assets/widow.png'
import profile from '../../assets/profile.png'

/**
 * @author Cristhiam Henao
 * @description This class handles the logic of the application and renders the main components
 */
class Profile extends Component {
  /**
   * @param {boolean} edit if true, the edit mode will be available
   * @param {boolean} showModal if true, it will show the modal to change profile picture 
   * @param {object} user object that contains information about user (name, description, favorite avenger, phone, profile picture)
   * @param {array} avengers list of objects that contains Avengers name (text) and picture (value)
   * @param {array} profilePics list of objects that contains profile pictures available order by name (text) and picture (value)
   * @param {array} posts lists of objects that contains each post content (text)
   */
  state = {
    edit: false,
    showModal : false,
    user:{
      name: 'Cristhiam Henao',
      description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non`,
      avenger: iron,
      phone: '(+57) 300 312 31 41',
      profile: profile

    },
    avengers:[
      {text:'Iron Man', value: iron},
      {text:'Captain America', value: america},
      {text:'Hawkeye', value: hawkeye},
      {text:'Hulk', value: hulk},
      {text:'Thor', value: thor},
      {text:'Black Widow', value: widow},
    ],
    profilePics:[
      {text:'Iron Man', value: iron},
      {text:'Captain America', value: america},
      {text:'Hawkeye', value: hawkeye},
      {text:'Hulk', value: hulk},
      {text:'Thor', value: thor},
      {text:'Black Widow', value: widow},
    ],
    posts:[
      {text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non'},
      {text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non'},
      {text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non'},
      {text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non'},
      {text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non'},
      {text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non'},
      {text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non'},
      {text: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum natus accusantium, ipsum optio molestiae saepe earum id dolorem, dolore rem ullam non'}
    ]
  }

  /**
   * @description it changes the edit mode to true 
   */
  edit = () => {
    this.setState({edit:true})
  }

  /**
   * @description it closes edition mode without save anything
   */
  cancel = () => {
    this.setState({edit:false})
  }

  /**
   * @description it shows modal to change profile picture
   */
  showModalFn = () => {
    this.setState({showModal:true})
  }

  /**
   * @description it updates image according to the one chosen in the edit profile picture modal
   * @param {object} e event triggered by the on click method
   */
  changeImage = () => e => {
    const user = {...this.state.user}
    user[e.target.name] = e.target.src

    this.setState({user, showModal:false})
  }

  /**
   * @description it updates user data when the edit mode is on
   * @param {object} e event triggered by the on click method
   */
  save = () => e => {
    e.preventDefault()
    const user = {...this.state.user}
    user[e.target.name.name] = e.target.name.value
    user[e.target.description.name] = e.target.description.value
    user[e.target.phone.name] = e.target.phone.value
    user[e.target.avenger.name] = e.target.avenger.value
    
    this.setState({
       user, edit: false
    })
  }

  /**
   * @description it renders user main screen and posts and if it's available, the edition mode
   */
  render(){
    const {user, edit, avengers, posts,
          profilePics, showModal
        } = this.state
    return(
      <div>
        <UserScreen edit={edit} user={user} editMode={this.edit}/>
        <Posts user={user} posts={posts}/>
        {
          edit && 
          <UserEdit 
            user={user} 
            avengers={avengers}
            onCancel={this.cancel}
            onSave={this.save}
            profilePics={profilePics}
            changeImage={this.changeImage}
            showModal={showModal}
            showModalFn={this.showModalFn}
          />
        }
      </div>
    )
  }
}

export default Profile