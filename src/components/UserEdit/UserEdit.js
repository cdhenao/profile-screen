import React, { Component } from 'react';
import './UserEdit.sass'

/**
 * @author Cristhiam Henao
 * @description This class handles the user edition
 */
class UserEdit extends Component {
  /**
  * @description It renders a edit view to let user changes its data
  */
  render(){
    const {user, onCancel, onSave, showModal, showModalFn,
          avengers, profilePics, changeImage} = this.props
    return (
      <div className="frontBackground">
        <div className="editMode">
          <form onSubmit={onSave()}>
            <div className="userScreen">
              <div onClick={showModalFn} className="profile">
                <p>Edit photo</p>
              </div>
              <div className="userInfo">
                <input name="name" type="text" className="name" defaultValue={user.name}/>
                <input name="phone" type="text" className="phone" defaultValue={user.phone}/>
              </div>
              <div className="imgAvenger">
                <select name="avenger" defaultValue={user.avenger}>
                {
                  avengers.map((value, key) => {
                    return(
                      <option key={key} value={value.value}>
                        {value.text}
                      </option>
                    )
                  })
                }
                </select>
              </div>
            </div>
            <div className="description">
              <textarea name="description" defaultValue={user.description}
                  rows="4" cols="60" maxLength="150"/>
              
            </div>
            <div className="buttons">
              <div className="edit">
                <input type="submit" value="Save"/> 
              </div>        
              <div className="edit">
                <input type="submit" value="Cancel" onClick={onCancel}/> 
              </div>
            </div>
          </form>
        </div>
        {showModal &&
          <div className="modalBckg">
            <div className="modal">
              <div className="title">
                <p>Select a profile picture</p>
              </div>
              <div className="pics">
              {
                profilePics.map((image,key)=>{
                  return(
                    <div key={key} className="pic">
                      <img name='profile'
                          alt="newpic" 
                          src={image.value}
                          onClick={changeImage()}
                      />
                    </div>
                  )
                })
              }
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}

export default UserEdit