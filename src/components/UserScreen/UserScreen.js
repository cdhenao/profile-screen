import React, { Component } from 'react';

import './UserScreen.sass'

/**
 * @author Cristhiam Henao
 * @description This class shows the user info
 */
class UserScreen extends Component{
  /**
  * @description It renders user information
  */
  render(){
    const {user, editMode, edit} = this.props
    return(
      <div className="background">
        <div className="userScreen">
          <img alt="profile" className="profile" src={user.profile}/>
          <div className="userInfo">
            <p className="name">{user.name}</p>
            <p className="phone">{user.phone}</p>
          </div>
          <div className={edit ? 'imgAvenger editOn' : 'imgAvenger'}>
            <img alt="avenger" className="avenger" src={user.avenger}/>
          </div>
        </div>
        <div className="description">{user.description}</div>
        <div className="edit">
          <input type="submit" onClick={editMode} value="Edit"/> 
        </div>
        
      </div>
    )
  }
}

export default UserScreen