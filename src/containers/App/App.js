import React, {Component} from 'react';
import Profile from '../../components/Profile/Profile'

/**
* @description This class contains the Profile components that renders everything in the app
*/
class App extends Component{
  
  render(){
    return (
      <div>
        <Profile/>
      </div>
    );
  }
}

export default App;
